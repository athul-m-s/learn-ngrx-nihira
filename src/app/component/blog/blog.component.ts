import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';
import { AppStateModel } from 'src/app/shared/store/app-state.model';
import {
  addBlog,
  updateBlog,
  deleteBlog,
  loadBlog,
} from 'src/app/shared/store/blog/blog.actions';
import { BlogModel, Blogs } from 'src/app/shared/store/blog/blog.model';
import {
  getBlog,
  getBlogById,
  getBlogInfo,
} from 'src/app/shared/store/blog/blog.selector';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
})
export class BlogComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  blogs: BlogModel[] = [];
  form: FormGroup;
  blogInfo: Blogs;
  errMsg: string;
  // constructor(private store: Store<{ blog: BlogModel[] }>) {}
  constructor(private store: Store<AppStateModel>, private fb: FormBuilder) {}
  ngOnInit(): void {
    this.store.dispatch(loadBlog());
    this.store.select(getBlogInfo).subscribe((b) => {
      this.blogInfo = b;
      this.errMsg = b.errorMessage;
    });
    // this.subscription = this.store.select(getBlog).subscribe((b) => {
    //   this.blogs = b;
    // });

    this.form = this.fb.group({
      id: this.fb.control('0'),
      title: this.fb.control(''),
      description: this.fb.control(''),
    });
  }

  addBlog() {
    const blogInput: BlogModel = {
      id: this.blogInfo?.blogList?.length + 1,
      title: this.form.get('title')?.value,
      description: this.form.get('description')?.value,
    };

    this.store.dispatch(addBlog({ blogInput: blogInput }));
  }

  deleteBlog(id: number) {
    this.store.dispatch(deleteBlog({ blogId: id }));
  }

  updateBlog(blog: BlogModel) {
    this.store.select(getBlogById(blog.id)).subscribe((x) => {
      let asd = x;
    });
    const b = { ...blog };
    b.description = 'updated';
    this.store.dispatch(updateBlog({ blogInput: b }));
  }

  ngOnDestroy(): void {}
}
