import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';
import { AppStateModel } from 'src/app/shared/store/app-state.model';
import { CounterModel } from 'src/app/shared/store/counter/counter.model';
import {
  getCounter,
  getName,
} from 'src/app/shared/store/counter/counter.selector';

@Component({
  selector: 'app-counter-display',
  templateUrl: './counter-display.component.html',
  styleUrls: ['./counter-display.component.css'],
})
export class CounterDisplayComponent implements OnInit, OnDestroy {
  counterDisplay: number;
  name: string;
  subscription: Subscription;
  subscription2: Subscription;
  subscription3: Subscription;
  counter$: Observable<CounterModel>;
  constructor(private store: Store<AppStateModel>) {}

  ngOnInit(): void {
    this.subscription = this.store.select('counter').subscribe((c) => {
      this.counterDisplay = c.counter;
      this.name = c.name;
    });
    this.subscription2 = this.store.select(getCounter).subscribe((c) => {
      this.counterDisplay = c;
    });

    this.subscription3 = this.store.select(getName).subscribe((c) => {
      this.name = c;
    });

    this.counter$ = this.store.select('counter');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.subscription2.unsubscribe();
    this.subscription3.unsubscribe();
  }
}
