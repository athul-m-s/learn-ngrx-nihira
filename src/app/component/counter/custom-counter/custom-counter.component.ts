import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { customIncrement } from 'src/app/shared/store/counter/counter.actions';
import { CounterModel } from 'src/app/shared/store/counter/counter.model';

@Component({
  selector: 'app-custom-counter',
  templateUrl: './custom-counter.component.html',
  styleUrls: ['./custom-counter.component.css'],
})
export class CustomCounterComponent {
  constructor(private store: Store<{ counter: CounterModel }>) {}
  onCustomIncrement() {
    this.store.dispatch(customIncrement({ value: 4200, action: 'subtract' }));
  }
}
