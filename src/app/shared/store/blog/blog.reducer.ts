import { createReducer, on } from '@ngrx/store';
import {
  addBlog,
  addBlogSuccess,
  deleteBlog,
  deleteBlogSuccess,
  loadBlog,
  loadBlogFailure,
  loadBlogSuccess,
  updateBlog,
  updateBlogSuccess,
} from './blog.actions';
import { initialBlogState } from './blog.state';

const _blogReducer = createReducer(
  initialBlogState,
  on(loadBlog, (state) => {
    return { ...state };
  }),
  on(loadBlogSuccess, (state, action) => {
    return { ...state, blogList: [...action.blogList], errorMessage: '' };
  }),
  on(loadBlogFailure, (state, action) => {
    return { ...state, blogList: [], errorMessage: action.errorMsg };
  }),
  //This action trigger the effect and it trigger success or fail action
  // on(addBlog, (state, action) => {
  //   let blog = { ...action.blogInput };
  //   blog.id = state.blogList.length + 1;
  //   return { ...state, blogList: [...state.blogList, blog] };
  // }),
  on(addBlogSuccess, (state, action) => {
    let blog = { ...action.blogInput };
    return { ...state, blogList: [...state.blogList, blog] };
  }),
  // on(deleteBlog, (state, action) => {
  //   const updatedBlogs = state.blogList.filter((el) => el.id != action.blogId);
  //   return { ...state, blogList: updatedBlogs };
  // }),
  on(deleteBlogSuccess, (state, action) => {
    const updatedBlogs = state.blogList.filter((el) => el.id != action.blogId);
    return { ...state, blogList: updatedBlogs };
  }),
  // on(updateBlog, (state, action) => {
  //   let blog = { ...action.blogInput };
  //   const updatedBlogs = state.blogList.map((el) => {
  //     return el.id === blog.id ? blog : el;
  //   });
  //   return { ...state, blogList: [...updatedBlogs] };
  // }),
  on(updateBlogSuccess, (state, action) => {
    let blog = { ...action.blogInput };
    const updatedBlogs = state.blogList.map((el) => {
      return el.id === blog.id ? blog : el;
    });
    return { ...state, blogList: [...updatedBlogs] };
  })
);

export function blogReducer(state: any, action: any) {
  return _blogReducer(state, action);
}
