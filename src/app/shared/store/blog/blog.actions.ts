import { createAction, props } from '@ngrx/store';
import { BlogModel } from './blog.model';

export const loadBlog = createAction('loadBlog');

export const addBlog = createAction(
  'addBlog',
  props<{ blogInput: BlogModel }>()
);
export const addBlogSuccess = createAction(
  'addBlogSuccess',
  props<{ blogInput: BlogModel }>()
);

export const updateBlog = createAction(
  'updateBlog',
  props<{ blogInput: BlogModel }>()
);
export const updateBlogSuccess = createAction(
  'updateBlogSuccess',
  props<{ blogInput: BlogModel }>()
);

export const deleteBlog = createAction(
  'deleteBlog',
  props<{ blogId: number }>()
);

export const deleteBlogSuccess = createAction(
  'deleteBlogSuccess',
  props<{ blogId: number }>()
);

export const loadBlogSuccess = createAction(
  '[BLOG] load success',
  props<{ blogList: BlogModel[] }>()
);
export const loadBlogFailure = createAction(
  '[BLOG] load failure',
  props<{ errorMsg: string }>()
);
