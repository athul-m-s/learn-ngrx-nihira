import { createFeatureSelector, createSelector } from '@ngrx/store';
import { BlogModel, Blogs } from './blog.model';

const getBlogState = createFeatureSelector<Blogs>('blog');

export const getBlog = createSelector(getBlogState, (state) => {
  return state.blogList;
});

export const getBlogById = (id: number) =>
  createSelector(getBlogState, (state) => {
    return state.blogList.find((b) => b.id === id);
  });

export const getBlogInfo = createSelector(getBlogState, (state) => {
  return state;
});
