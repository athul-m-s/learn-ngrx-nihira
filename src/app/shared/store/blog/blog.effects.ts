import { Injectable } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { MasterService } from '../../master.service';
import { createEffect } from '@ngrx/effects';
import { exhaustMap, map, catchError, EMPTY, of } from 'rxjs';
import {
  addBlog,
  addBlogSuccess,
  deleteBlog,
  deleteBlogSuccess,
  loadBlog,
  loadBlogFailure,
  loadBlogSuccess,
  updateBlog,
  updateBlogSuccess,
} from './blog.actions';

@Injectable()
export class BlogEffects {
  constructor(private action$: Actions, private service: MasterService) {}

  public _blog = createEffect(() => {
    return this.action$.pipe(
      ofType(loadBlog),
      exhaustMap((action) => {
        return this.service.getAllBlogs().pipe(
          map((data) => {
            return loadBlogSuccess({ blogList: data });
          }),
          catchError((err) => {
            return of(loadBlogFailure({ errorMsg: err.message }));
            // return EMPTY;
          })
        );
      })
    );
  });

  public _AddBlog = createEffect(() => {
    return this.action$.pipe(
      ofType(addBlog),
      exhaustMap((action) => {
        return this.service.addBlogs(action.blogInput).pipe(
          map((data) => {
            return addBlogSuccess({ blogInput: data });
          }),
          catchError((err) => {
            return of(loadBlogFailure({ errorMsg: err.message }));
            // return EMPTY;
          })
        );
      })
    );
  });

  public _UpdateBlog = createEffect(() => {
    return this.action$.pipe(
      ofType(updateBlog),
      exhaustMap((action) => {
        return this.service.updateBlogs(action.blogInput).pipe(
          map((data) => {
            return updateBlogSuccess({ blogInput: action.blogInput });
          }),
          catchError((err) => {
            return of(loadBlogFailure({ errorMsg: err.message }));
            // return EMPTY;
          })
        );
      })
    );
  });

  //IF multiple actions need to trigger use switchmap
  // _UpdateBlog = createEffect(() =>
  //   this.action$.pipe(
  //     ofType(updateblog),
  //     switchMap((action) =>
  //       this.service.UpdateBlog(action.bloginput).pipe(
  //         switchMap((res) =>
  //           of(
  //             updateblogsuccess({ bloginput: action.bloginput }),
  //             loadspinner({ isloaded: false }),
  //             ShowAlert({
  //               message: 'Updated successfully.',
  //               actionresult: 'pass',
  //             })
  //           )
  //         ),
  //         catchError((_error) =>
  //           of(
  //             ShowAlert({
  //               message: 'Update Failed - Due to' + _error.message,
  //               actionresult: 'fail',
  //             }),
  //             loadspinner({ isloaded: false })
  //           )
  //         )
  //       )
  //     )
  //   )
  // );

  public _DeleteBlog = createEffect(() => {
    return this.action$.pipe(
      ofType(deleteBlog),
      exhaustMap((action) => {
        return this.service.deleteBlogs(action.blogId).pipe(
          map((data) => {
            return deleteBlogSuccess({ blogId: action.blogId });
          }),
          catchError((err) => {
            return of(loadBlogFailure({ errorMsg: err.message }));
            // return EMPTY;
          })
        );
      })
    );
  });
}
