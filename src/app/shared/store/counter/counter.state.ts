import { CounterModel } from './counter.model';

export const initialState: CounterModel = {
  counter: 0,
  name: 'Test Name',
};
export { CounterModel };
