import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BlogModel } from './store/blog/blog.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MasterService {
  constructor(private http: HttpClient) {}

  getAllBlogs(): Observable<BlogModel[]> {
    return this.http.get<BlogModel[]>('http://localhost:3000/blogs');
  }

  addBlogs(data: BlogModel): Observable<BlogModel> {
    return this.http.post<BlogModel>('http://localhost:3000/blogs', data);
  }

  updateBlogs(data: BlogModel): Observable<BlogModel> {
    return this.http.put<BlogModel>(
      `http://localhost:3000/blogs/${data.id}`,
      data
    );
  }

  deleteBlogs(id: number): Observable<BlogModel> {
    return this.http.delete<BlogModel>(`http://localhost:3000/blogs/${id}`);
  }
}
